<?
	if(invalidSession()) die(dueTo("No Session Id"));
	
	include 'configuration.php';
	
	if(invalidUser()) die(dueTo("Incorrect Token Id"));
	
    switch($_SERVER['REQUEST_METHOD']) {
    case 'GET': case 'POST': executeSecurityLayer(); break;
    default: die(dueTo($_SERVER['REQUEST_METHOD']." not allowed in Session"));
    }
	
	function executeSecurityLayer()
	{
		$session = isset($_POST['session']) ? $_POST['session'] : $_GET['session'];
		$key = isset($_POST['key']) ? $_POST['key'] : $_GET['key'];

		switch($session) {
	    case $_ENV['drug']: $table = "drug"; break;
	    case $_ENV['doctor']: $table = "doctor"; break;
	    case $_ENV['patient']: $table = "patient"; break;
	    case $_ENV['address']: $table = "address"; break;
	    case $_ENV['pharmacist']: $table = "pharmacist"; break;
	    case $_ENV['insuranceco']: $table = "insuranceco"; break;
	    case $_ENV['prescription']: $table = "prescription"; break;
	    // case $_ENV['securitytoken']: $table = "securitytoken"; break; // NOT ALLOWED FOR SECURITY REASONS, BUT GOOD FOR TESTING
	    default: break;
	    }
	
		requestJsonFrom($table, $key);
	}
 
	function invalidUser()
	{
		if(invalidToken()) return true;
		
		$token = isset($_POST['token']) ? $_POST['token'] : $_GET['token'];
		$user = json_decode(assignJsonFrom("securitytoken", $token));
		$objs = $user->securitytoken;
				
		if(count($objs) != 1) return true;
		
		$_ENV['UserName'] = $objs[0]->Username;

		return invalidUserID();
	}
	
	function invalidUserID()
	{
		return !isset($_ENV['UserName']);
	}
	
	function invalidToken()
	{
		return !isset($_POST['token']) && !isset($_GET['token']);
	}
	
	function invalidSession()
	{
		return !isset($_POST['session']) && !isset($_GET['session']);
	}
	
	function dueTo($message)
	{
		return json_encode(array("success" => false, "reason" => $message));
	}
	
	function getSelectionDataFrom($table, $key)
	{
		switch($table) {
		case "doctor": return listPatientsPrescriptionsForDoctor($key);
		case "securitytoken": return "SELECT * FROM securitytoken WHERE Hash = '$key'";//'d0c53e1fcaa78174a5adff5b57294d'
		default: return "SELECT * FROM $table";
		}
	}
	
	function listPatientsPrescriptionsForDoctor()
	{
		if(invalidUserID()) die(dueTo("Incorrect Data, please see your Database Administrator"));
		return "SELECT  CONCAT_WS(' ', pa.FirstName, pa.LastSurname) AS Patient, 
				p.ID AS PrescriptionNumber,
				d.Name AS Drug,
				p.Amount,
				p.Dosage,
				p.RefillAmount AS Refills, 
				DATE_FORMAT(p.OrderDate, '%M %e, %Y') AS OrderDate,
				CONCAT_WS(' ', ph.FirstName, ph.LastSurname) AS Pharmacist,
				CONCAT_WS(' ', dr.FirstName, dr.LastSurname) AS Doctor
		FROM doctor dr
		INNER JOIN securitytoken st ON st.ID = dr.securitytokenID
		INNER JOIN prescription p ON p.doctorID = dr.ID
		INNER JOIN patient pa ON pa.ID = p.patientID
		INNER JOIN drug d ON d.ID = p.drugID
		INNER JOIN pharmacist ph ON ph.ID = p.pharmacistID
		WHERE st.Username = '".$_ENV['UserName']."';";
	}
	
	function requestJsonFrom($table, $key)
	{
		$value = assignJsonFrom($table, $key);
		if($value != "Invalid Session Id") echo $value;
		else die(dueTo($value));
	}
	
	function assignJsonFrom($table, $key)
	{
		if($table)
		{			
			$objs = array();
			$sqldata = mysql_query(getSelectionDataFrom($table, $key));
			
			while($o = mysql_fetch_assoc($sqldata)) { $objs[] = $o; }
			return json_encode(array("success" => count($objs) != 0, $table => $objs));
			
		} else return "Invalid Session Id";
	}
?>