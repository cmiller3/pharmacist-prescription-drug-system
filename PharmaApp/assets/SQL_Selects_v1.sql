-- List all Patient/prescription info by Dr username
SELECT  CONCAT_WS(' ', pa.FirstName, pa.LastSurname) AS Patient, 
		p.ID AS PrescriptionNumber,
		d.Name AS Drug,
		p.Amount,
		p.Dosage,
		p.RefillAmount AS Refills, 
		DATE_FORMAT(p.OrderDate, '%M %e, %Y') AS OrderDate,
		CONCAT_WS(' ', ph.FirstName, ph.LastSurname) AS Pharmacist,
		CONCAT_WS(' ', dr.FirstName, dr.LastSurname) AS Doctor
FROM doctor dr
INNER JOIN securitytoken st ON st.ID = dr.securitytokenID
INNER JOIN prescription p ON p.doctorID = dr.ID
INNER JOIN patient pa ON pa.ID = p.patientID
INNER JOIN drug d ON d.ID = p.drugID
INNER JOIN pharmacist ph ON ph.ID = p.pharmacistID
WHERE st.Username = 'cmiller';

-- List all patients by Dr username
SELECT  DISTINCT CONCAT_WS(' ', pa.FirstName, pa.LastSurname) AS Patient,
		DATE_FORMAT(pa.Birthdate, '%M %e, %Y') AS BirthDate,
		CASE pa.Gender
			WHEN 'M' then 'Male'
			ELSE 'Female'
		END AS Gender,
		CONCAT(a.StreetNumberName, ', ', a.City, ', ',a.StateAbbreviation, ' ', a.PostalCode )AS Address,
		CONCAT('(', SUBSTRING(pa.TelephoneNumber, 1, 3), ') ', SUBSTRING(pa.TelephoneNumber, 4, 3), '-', SUBSTRING(pa.TelephoneNumber, 7)) AS TelephoneNumber,
		pa.Email,
		ic.Name AS Insurance,
		pa.Contact,
		CONCAT('(', SUBSTRING(pa.ContactPhone, 1, 3), ') ', SUBSTRING(pa.ContactPhone, 4, 3), '-', SUBSTRING(pa.ContactPhone, 7)) AS ContactPhone
FROM patient pa
INNER JOIN address a ON a.ID = pa.addressID
INNER JOIN insuranceco ic ON ic.ID = pa.insurancecoID
INNER JOIN prescription p ON p.patientID = pa.ID
INNER JOIN doctor d ON d.ID = p.doctorID
INNER JOIN securitytoken st ON st.ID = d.securitytokenID
WHERE st.Username = 'cmiller';