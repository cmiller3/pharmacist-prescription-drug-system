#Pharmacist Prescription Drug System
The Pharmacist Prescription Drug System is a final project for the Software Architecture Course (SWE 3633). 

##Team Members
The project is currently led by Kyle Bechtel, Karl Fehd, Brian Davis and Christopher Miller.

##Project Purpose
The system is designed to suit the needs of a pharmacist during their day to day operations, interacting with patients and doctors. The System is run 
from a php server and with the session and token required parameters, it provides a layer of secure server side access for Drs and Pharmacists. There will only be two 
client types, and one is the Web Interface and the other is the Android Client. If you want to see how the API works first hand, please visit the live site for 
this project, which can be found [here](http://the-mac.us/prescription/index.htm).

## Android Pharmacist Prescription Drug Client
Please feel free to take a look at the current version of the Pharma.apk (Android Package) file to test out the android application's completed modules.
###The Pharma Android app is under [downloads](https://bitbucket.org/cmiller3/pharmacist-prescription-drug-system/downloads/Pharma.apk)

## Web Admin Prescription Drug Client
Please feel free to take a look at the current version of the http://the-mac.us/prescription/site to test out the web application's completed modules.

